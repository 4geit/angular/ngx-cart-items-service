import { Inject, Injectable, Optional } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

import { FIXTURES, BASE_PATH } from './variables';

@Injectable()
export class NgxCartItemsService {

  protected fixtures = false;
  basePath = '/product';
  items = [];
  total = 0;

  constructor(
    private snackBar: MdSnackBar,
    private productsService: NgxMarketplaceProductsService,
    @Optional()@Inject(FIXTURES) fixtures: boolean,
    @Optional()@Inject(BASE_PATH) basePath: string,
  ) {
    if (fixtures) { this.fixtures = fixtures; }
    if (basePath) { this.basePath = basePath; }
    if (this.fixtures) {
      this.items = this.productsService.items.map((item, i) => Object({
        slug: item.slug,
        name: item.name,
        description: item.name,
        price: item.price,
        quantity: i + 1,
        total: item.price * (i + 1)
      }));
      this.computeTotal();
    }
  }

  computeTotal() {
    this.total = this.items.map(i => i.total).reduce((a, b) => a + b, 0);
    console.log(this.total);
    console.log(this.items);
  }

  addItem(item: any) {
    if (~this.items.findIndex(i => i.slug === item.slug)) {
      this.snackBar.open('Item already added to cart', 'Hide', {
        duration: 2000
      });
      return;
    }
    this.items.push(item);
    this.computeTotal();
    this.snackBar.open('Item added to cart', 'Hide', {
      duration: 2000
    });
  }

  removeItem(item: any) {
    const index = this.items.findIndex(i => i.slug === item.slug);
    if (~index) {
      this.items.splice(index, 1);
      this.computeTotal();
      this.snackBar.open('Item removed from cart', 'Hide', {
        duration: 2000
      });
      return;
    }
    this.snackBar.open('Item does not exist in the cart', 'Hide', {
      duration: 2000
    });
  }

  computeItem(item: any) {
    const index = this.items.findIndex(i => i.slug === item.slug);
    if (~index) {
      this.items[index].total = item.price * item.quantity;
      this.computeTotal();
      this.snackBar.open('Item updated', 'Hide', {
        duration: 2000
      });
      return;
    }
    this.snackBar.open('Item does not exist in the cart', 'Hide', {
      duration: 2000
    });
  }

}
