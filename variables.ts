import { InjectionToken } from '@angular/core';

export const FIXTURES = new InjectionToken<boolean>('fixtures');
export const BASE_PATH = new InjectionToken<string>('basePath');
