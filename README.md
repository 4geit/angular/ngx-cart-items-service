# @4geit/ngx-cart-items-service [![npm version](//badge.fury.io/js/@4geit%2Fngx-cart-items-service.svg)](//badge.fury.io/js/@4geit%2Fngx-cart-items-service)

---

service that handles cart items

## Installation

1. A recommended way to install ***@4geit/ngx-cart-items-service*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-cart-items-service) package manager using the following command:

```bash
npm i @4geit/ngx-cart-items-service --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-cart-items-service
```

2. You need to import the `NgxCartItemsService` service in the module you want to use it. For instance `app.module.ts` as follows:

```js
import { NgxCartItemsService } from '@4geit/ngx-cart-items-service';
```

And you also need to add the `NgxCartItemsService` service within the `@NgModule` decorator as part of the `providers` list.

```js
@NgModule({
  // ...
  providers: [
    // ...
    NgxCartItemsService,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. This service allows to override some variables. You can change a variable value as shown in the example below. First you will need to import those variables:

```js
import { VAR1 } from '@4geit/ngx-cart-items-service';
```

And set the variable value in the `providers` list as below:

```js
@NgModule({
  // ...
  providers: [
    // ...
    { provide: VAR1, useValue: 'VALUE1' },
    // ...
  ],
  // ...
})
export class AppModule { }
```

Here are the list of the variables this service allows you to override:

* VAR1
* VAR2
* VAR3
