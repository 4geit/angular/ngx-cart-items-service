import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxCartItemsService } from './ngx-cart-items.service';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
  ],
  providers: [
    NgxCartItemsService
  ],
  exports: [
  ]
})
export class NgxCartItemsModule { }
