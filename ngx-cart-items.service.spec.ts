import { TestBed, inject } from '@angular/core/testing';

import { NgxCartItemsService } from './cart-items.service';

describe('NgxCartItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxCartItemsService]
    });
  });

  it('should be created', inject([NgxCartItemsService], (service: NgxCartItemsService) => {
    expect(service).toBeTruthy();
  }));
});
